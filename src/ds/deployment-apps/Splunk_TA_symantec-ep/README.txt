Splunk Add-on for Symantec Endpoint Protection version 2.0.1

Copyright (C) 2009-2015 Splunk Inc. All Rights Reserved.
 
The Splunk Add-on for Symantec Endpoint Protection allows a Splunk® Enterprise administrator to collect logs from Symantec Endpoint Protection Manager over dump files. You can collect the following log files:
* Server Administration	
* Application and Device Control	
* Server Client	
* Server Policy	
* Server System	
* Client Packet	
* Client Proactive Threat	
* Client Risk	
* Client Scan	
* Client Security	
* Client System	
* Client Traffic

After Splunk Enterprise indexes the events, you can consume the data using the prebuilt dashboard panels included with the add-on. This add-on provides the inputs and CIM-compatible knowledge to use with other Splunk Enterprise apps, such as the Splunk App for Enterprise Security and the Splunk App for PCI Compliance.

Documentation for this add-on is located at: http://docs.splunk.com/Documentation/AddOns/latest/SymantecEP/About

For installation and set-up instructions, refer to the Installation and Configuration section: http://docs.splunk.com/Documentation/AddOns/latest/SymantecEP/Hardwareandsoftwarerequirements

For release notes, refer to the Release notes section: http://docs.splunk.com/Documentation/AddOns/latest/SymantecEP/Releasenotes